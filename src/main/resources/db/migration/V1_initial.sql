CREATE TABLE IF NOT EXISTS Admins (
    AdminID SERIAL PRIMARY KEY,
    Username VARCHAR(50) UNIQUE NOT NULL,
    PasswordHash VARCHAR(255) NOT NULL,
    UserRole VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS AddressTypes (
    AddressTypeID SERIAL PRIMARY KEY,
    Code INT NOT NULL UNIQUE,
    Description VARCHAR(50) NOT NULL -- 'email-1', 'phone-2', 'postal-3'
);

CREATE TABLE IF NOT EXISTS NotifyTypes (
    NotifyTypeID SERIAL PRIMARY KEY,
    Code INT NOT NULL UNIQUE,
    Description VARCHAR(50) NOT NULL -- 'SMS-1', 'email-2', 'post-3'
);

CREATE TABLE IF NOT EXISTS StatusTypes (
    StatusTypeID SERIAL PRIMARY KEY,
    Code INT NOT NULL UNIQUE,
    Description VARCHAR(50) NOT NULL -- 'Delivered-1', 'Failed-2', 'Pending-3'
);

CREATE TABLE IF NOT EXISTS Customers (
    CustomerID SERIAL PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Addresses (
    AddressID SERIAL PRIMARY KEY,
    CustomerID INT REFERENCES Customers(CustomerID),
    AddressTypeCode INT REFERENCES AddressTypes(AddressTypeCode),
    AddressText VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS OptInSettings (
    OptInSettingsID SERIAL PRIMARY KEY,
    CustomerID INT REFERENCES Customers(CustomerID),
    NotifyTypeCode INT REFERENCES NotifyTypes(NotifyTypeCode)
);

CREATE TABLE IF NOT EXISTS Notifications (
    NotificationID SERIAL PRIMARY KEY,
    CustomerID INT REFERENCES Customers(CustomerID),
    NotifyTypeCode INT REFERENCES NotifyTypes(NotifyTypeCode),
    StatusTypeCode INT REFERENCES StatusTypes(StatusTypeCode),
    SentDate timestamp,
    UpdateDate timestamp,
    Content TEXT
);
