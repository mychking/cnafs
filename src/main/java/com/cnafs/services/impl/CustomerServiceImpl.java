package com.cnafs.services.impl;

import com.cnafs.exceptions.EntityNotFoundException;
import com.cnafs.models.Customer;
import com.cnafs.repos.CustomerRepository;
import com.cnafs.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Override
    public List<Customer> findAllCustomers() {
        List<Customer> customers = customerRepository.findAll();
        customers.forEach(Customer::calculateSuccessPercent);
        return customers;
    }

    @Override
    public List<Customer> findCustomers(String name, String contactInformation, String notificationPreferences, String sort, String order) {
        // TODO search, filter, and sort logic
        //return customerRepository.findAndSortCustomers(name, contactInformation, notificationPreferences, sort, order);
        return null;
    }

    @Override
    public Customer saveCustomer(Customer customer) {
        return customerRepository.saveAndFlush(customer);
    }

    @Override
    public Customer findByID(Long id) {
        var customer = customerRepository.findById(id)
                .orElseThrow(() -> EntityNotFoundException.from(id, Customer.class));
        customer.calculateSuccessPercent();
        return customer;
    }

    @Override
    public Customer updateCustomer(Long id, Customer customer) {
        Customer existingCustomer = findByID(id);
        existingCustomer.setFirstname(customer.getFirstname());
        existingCustomer.setLastname(customer.getLastname());
        return customerRepository.save(existingCustomer);
    }

    @Override
    @Transactional
    public void deleteCustomer(Long id) {
        customerRepository.deleteById(id);
    }
}
