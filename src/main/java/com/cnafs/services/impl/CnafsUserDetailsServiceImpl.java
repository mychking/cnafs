package com.cnafs.services.impl;

import com.cnafs.repos.AdminRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CnafsUserDetailsServiceImpl implements UserDetailsService {

    private final String ERROR_NOTFOUND = "User name='%s' not found.";

    private final AdminRepository adminRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return adminRepository.findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException(String.format(ERROR_NOTFOUND, username)));
    }
}
