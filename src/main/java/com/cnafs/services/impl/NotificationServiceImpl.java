package com.cnafs.services.impl;

import com.cnafs.models.Notification;
import com.cnafs.repos.NotificationRepository;
import com.cnafs.services.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;

    @Override
    public List<Notification> findAllNotifications() {
        return notificationRepository.findAll();
    }

    @Override
    public List<Notification> findAllNotificationsByCustomerId(Long customerId) {
        return notificationRepository.findByCustomer_customerid(customerId);
    }
}