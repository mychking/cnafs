package com.cnafs.services.impl;

import com.cnafs.exceptions.EntityNotFoundException;
import com.cnafs.models.Customer;
import com.cnafs.models.OptinSetting;
import com.cnafs.repos.OptinSettingRepository;
import com.cnafs.services.OptinSettingService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class OptinSettingServiceImpl implements OptinSettingService {
    private final OptinSettingRepository optinSettingRepository;

    @Override
    public List<OptinSetting> findAllOptinSettingsByCustomerId(Long customerId) {
        return optinSettingRepository.findByCustomer_customerid(customerId);
    }

    @Override
    public OptinSetting saveOptinSetting(OptinSetting optinSetting) {
        return optinSettingRepository.saveAndFlush(optinSetting);
    }

    @Override
    public OptinSetting findOptinSettingById(Long optinSettingId) {
        return optinSettingRepository.findById(optinSettingId)
                .orElseThrow(() -> EntityNotFoundException.from(optinSettingId, Customer.class));
    }

    @Override
    @Transactional
    public void deleteOptinSetting(Long optinSettingId) {
        optinSettingRepository.deleteById(optinSettingId);
    }
}
