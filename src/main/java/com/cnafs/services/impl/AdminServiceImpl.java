package com.cnafs.services.impl;

import com.cnafs.models.Admin;
import com.cnafs.repos.AdminRepository;
import com.cnafs.services.AdminService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {
    private final String ERROR_NOTFOUND = "User name='%d' not found.";
    private final String ERROR_WRONGPSWD = "Password was wrong";

    private final AdminRepository adminRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;

    @Override
    public List<Admin> findAllAdmins() {
        return adminRepository.findAll();
    }

    @Override
    public Admin saveAdmin(Admin admin) {
        return adminRepository.saveAndFlush(admin);
    }

    @Override
    public String signIn(String username, String password, HttpServletRequest request) {
        var admin = adminRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(ERROR_NOTFOUND));
        if (!passwordEncoder.matches(password, admin.getPassword())) {
            return ERROR_WRONGPSWD;
        }
        var authReq = new UsernamePasswordAuthenticationToken(admin.getUsername(), password);
        var auth = authenticationManager.authenticate(authReq);
        var sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
        var session = request.getSession(true);
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);
        return "";
    }
}
