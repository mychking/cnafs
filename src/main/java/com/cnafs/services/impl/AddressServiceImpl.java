package com.cnafs.services.impl;

import com.cnafs.exceptions.EntityNotFoundException;
import com.cnafs.models.Address;
import com.cnafs.repos.AddressRepository;
import com.cnafs.services.AddressService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    @Override
    public List<Address> findAllAddressesByCustomerId(Long customerId) {
        return addressRepository.findByCustomer_customerid(customerId);
    }

    @Override
    public Address saveAddress(Address address) {
        return addressRepository.saveAndFlush(address);
    }

    @Override
    public Address findAddressById(Long addressId) {
        return addressRepository.findById(addressId)
                .orElseThrow(() -> EntityNotFoundException.from(addressId, Address.class));
    }

    @Override
    public Address updateAddress(Long addressId, Address address) {
        Address existingAddress = findAddressById(addressId);
        existingAddress.setAddressTypeCode(address.getAddressTypeCode());
        existingAddress.setAddressText(address.getAddressText());
        return addressRepository.save(existingAddress);
    }

    @Override
    @Transactional
    public void deleteAddress(Long addressId) {
        addressRepository.deleteById(addressId);
    }
}
