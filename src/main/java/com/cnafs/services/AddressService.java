package com.cnafs.services;

import com.cnafs.models.Address;

import java.util.List;

public interface AddressService {
    List<Address> findAllAddressesByCustomerId(Long customerId);

    Address saveAddress(Address address);

    Address findAddressById(Long addressId);

    Address updateAddress(Long addressId, Address address);

    void deleteAddress(Long addressId);
}
