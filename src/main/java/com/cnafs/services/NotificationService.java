package com.cnafs.services;

import com.cnafs.models.Notification;

import java.util.List;

public interface NotificationService {
    List<Notification> findAllNotifications();
    List<Notification> findAllNotificationsByCustomerId(Long customerId);
}
