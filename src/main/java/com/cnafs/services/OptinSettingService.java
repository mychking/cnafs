package com.cnafs.services;

import com.cnafs.models.OptinSetting;

import java.util.List;

public interface OptinSettingService {
    List<OptinSetting> findAllOptinSettingsByCustomerId(Long customerId);

    OptinSetting saveOptinSetting(OptinSetting optinSetting);

    OptinSetting findOptinSettingById(Long optinSettingId);

    void deleteOptinSetting(Long optinSettingId);
}
