package com.cnafs.services;

import com.cnafs.models.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> findAllCustomers();

    List<Customer> findCustomers(String name, String contactInformation, String notificationPreferences, String sort, String order);

    Customer saveCustomer(Customer customer);

    Customer findByID(Long id);

    Customer updateCustomer(Long id, Customer customer);

    void deleteCustomer(Long id);
}
