package com.cnafs.services;

import com.cnafs.models.Admin;
import jakarta.servlet.http.HttpServletRequest;

import java.util.List;

public interface AdminService {

    List<Admin> findAllAdmins();

    Admin saveAdmin(Admin admin);

    String signIn(String username, String password, HttpServletRequest request);

}
