package com.cnafs.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String errorMessage) {
        super(errorMessage);
    }

    public static EntityNotFoundException from(Object entityId, Class<?> clazz) {
        var message = String.format("%s with id = %s not found", clazz.getSimpleName(), entityId);
        return new EntityNotFoundException(message);
    }
}
