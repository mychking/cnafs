package com.cnafs.config;

import com.cnafs.models.AddressType;
import com.cnafs.models.Admin;
import com.cnafs.models.NotifyType;
import com.cnafs.models.Role;
import com.cnafs.models.StatusType;
import com.cnafs.repos.AddressTypeRepository;
import com.cnafs.repos.AdminRepository;
import com.cnafs.repos.NotifyTypeRepository;
import com.cnafs.repos.StatusTypeRepository;
import com.cnafs.services.AdminService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class SeedDataConfig implements CommandLineRunner {
    private final AdminRepository adminRepository;
    private final AdminService adminService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final AddressTypeRepository addressTypeRepository;
    private final NotifyTypeRepository notifyTypeRepository;
    private final StatusTypeRepository statusTypeRepository;

    @Override
    public void run(String... args) throws Exception {
        // Initialize user (TODO - for example and test only: create one SUPER and one ADMIN)
        if (adminRepository.count() == 0) {
            var user = Admin
                    .builder()
                    .username("super")
                    .password(passwordEncoder.encode("super"))
                    .role(Role.ROLE_SUPER)
                    .build();
            adminService.saveAdmin(user);
            log.debug("User with ROLE_SUPER created - {}", user);
            user = Admin
                    .builder()
                    .username("admin")
                    .password(passwordEncoder.encode("admin"))
                    .role(Role.ROLE_ADMIN)
                    .build();
            adminService.saveAdmin(user);
            log.debug("User with ROLE_ADMIN created - {}", user);
        }

        // Initialize AddressTypes
        if (addressTypeRepository.count() == 0) {
            addressTypeRepository.save(AddressType.builder().code(1).description("Email").build());
            addressTypeRepository.save(AddressType.builder().code(2).description("Phone").build());
            addressTypeRepository.save(AddressType.builder().code(3).description("Postal").build());
            log.debug("AddressType entries created.");
        }

        // Initialize NotifyTypes
        if (notifyTypeRepository.count() == 0) {
            notifyTypeRepository.save(NotifyType.builder().code(1).description("SMS").build());
            notifyTypeRepository.save(NotifyType.builder().code(2).description("Email").build());
            notifyTypeRepository.save(NotifyType.builder().code(3).description("Post").build());
            log.debug("NotifyType entries created.");
        }

        // Initialize StatusTypes
        if (statusTypeRepository.count() == 0) {
            statusTypeRepository.save(StatusType.builder().code(1).description("Delivered").build());
            statusTypeRepository.save(StatusType.builder().code(2).description("Failed").build());
            statusTypeRepository.save(StatusType.builder().code(3).description("Pending").build());
            log.debug("StatusType entries created.");
        }

    }
}
