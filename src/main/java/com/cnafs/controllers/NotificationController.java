package com.cnafs.controllers;

import com.cnafs.models.Notification;
import com.cnafs.services.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/customers")
public class NotificationController {

    private final NotificationService notificationService;

    @GetMapping("/notifications")
    public List<Notification> findAllNotifications() {
        return notificationService.findAllNotifications();
    }

    @GetMapping("/{customerId}/notifications")
    public List<Notification> findAllNotificationsByCustomerId(@PathVariable Long customerId) {
        return notificationService.findAllNotificationsByCustomerId(customerId);
    }
}
