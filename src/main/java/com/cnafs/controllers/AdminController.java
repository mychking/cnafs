package com.cnafs.controllers;

import com.cnafs.models.Admin;
import com.cnafs.services.AdminService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/users")
public class AdminController {
    private final AdminService adminService;

    @GetMapping
    public List<Admin> findAllAdmins() {
        return adminService.findAllAdmins();
    }
}
