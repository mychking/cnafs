package com.cnafs.controllers;

import com.cnafs.models.OptinSetting;
import com.cnafs.services.OptinSettingService;
import com.cnafs.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/customers/{customerId}/preferences")
public class OptinSettingController {

    private final OptinSettingService optinSettingService;
    private final CustomerService customerService;

    @GetMapping
    public List<OptinSetting> findAllOptinSettingsByCustomerId(@PathVariable Long customerId) {
        return optinSettingService.findAllOptinSettingsByCustomerId(customerId);
    }

    @PostMapping
    public OptinSetting saveOptinSetting(@PathVariable Long customerId, @RequestBody OptinSetting optinSetting) {
        optinSetting.setCustomer(customerService.findByID(customerId)); // Set the Customer object in the OptinSetting
        return optinSettingService.saveOptinSetting(optinSetting);
    }

    @GetMapping("/{preferenceId}")
    public OptinSetting findOptinSettingById(@PathVariable Long preferenceId) {
        return optinSettingService.findOptinSettingById(preferenceId);
    }

    @DeleteMapping("/{preferenceId}")
    public void deleteOptinSetting(@PathVariable Long preferenceId) {
        optinSettingService.deleteOptinSetting(preferenceId);
    }
}
