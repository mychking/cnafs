package com.cnafs.controllers;

import com.cnafs.models.Customer;
import com.cnafs.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@RestController
//@Controller
@AllArgsConstructor
@RequestMapping("/api/customers")
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public List<Customer> findAllCustomersJson() {
        return customerService.findAllCustomers();
    }

    @GetMapping(produces = { MediaType.TEXT_HTML_VALUE })
    public ModelAndView findAllCustomers() {
        List<Customer> customers = customerService.findAllCustomers();
        var model = Map.of("customers", customers);
        return new ModelAndView("customers", model);
    }

    @PostMapping
    public Customer saveCustomer(@RequestBody Customer customer) {
        return customerService.saveCustomer(customer);
    }

    @GetMapping("/{id}")
    public Customer findByID(@PathVariable Long id) {
        return customerService.findByID(id);
    }

    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable Long id, @RequestBody Customer customer) {
        return customerService.updateCustomer(id, customer);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable Long id){
        customerService.deleteCustomer(id);
    }
}
