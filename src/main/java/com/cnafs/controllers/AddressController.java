package com.cnafs.controllers;

import com.cnafs.models.Address;
import com.cnafs.services.AddressService;
import com.cnafs.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/customers/{customerId}/addresses")
public class AddressController {

    private final AddressService addressService;
    private final CustomerService customerService;

    @GetMapping
    public List<Address> findAllAddressesByCustomerId(@PathVariable Long customerId) {
        return addressService.findAllAddressesByCustomerId(customerId);
    }

    @PostMapping
    public Address saveAddress(@PathVariable Long customerId, @RequestBody Address address) {
        address.setCustomer(customerService.findByID(customerId)); // Set the Customer object in the Address
        return addressService.saveAddress(address);
    }

    @GetMapping("/{addressId}")
    public Address findAddressById(@PathVariable Long addressId) {
        return addressService.findAddressById(addressId);
    }

    @PutMapping("/{addressId}")
    public Address updateAddress(@PathVariable Long addressId, @RequestBody Address address) {
        return addressService.updateAddress(addressId, address);
    }

    @DeleteMapping("/{addressId}")
    public void deleteAddress(@PathVariable Long addressId) {
        addressService.deleteAddress(addressId);
    }
}
