package com.cnafs.repos;

import com.cnafs.models.AddressType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressTypeRepository extends JpaRepository<AddressType, Long> {
    Optional<AddressType> findByCode(Integer code);
}