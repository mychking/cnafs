package com.cnafs.repos;

import com.cnafs.models.NotifyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NotifyTypeRepository extends JpaRepository<NotifyType, Long> {
    Optional<NotifyType> findByCode(Integer code);
}
