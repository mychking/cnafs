package com.cnafs.repos;

import com.cnafs.models.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, Long> {

    public List<Notification> findByCustomer_customerid(Long customerId);

}
