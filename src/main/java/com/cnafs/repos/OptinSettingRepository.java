package com.cnafs.repos;

import com.cnafs.models.OptinSetting;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OptinSettingRepository extends JpaRepository<OptinSetting, Long> {

    public List<OptinSetting> findByCustomer_customerid(Long customerId);

}
