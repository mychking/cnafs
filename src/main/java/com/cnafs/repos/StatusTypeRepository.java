package com.cnafs.repos;

import com.cnafs.models.StatusType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StatusTypeRepository extends JpaRepository<StatusType, Long> {
    Optional<StatusType> findByCode(Integer code);
}
