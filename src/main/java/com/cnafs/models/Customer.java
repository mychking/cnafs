package com.cnafs.models;

import lombok.*;
import jakarta.persistence.*;

import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long customerid;

    @Column(name = "firstname", nullable = false, length = 50)
    private String firstname;

    @Column(name = "lastname", nullable = false, length = 50)
    private String lastname;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Address> addresses;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<OptinSetting> optinSettings;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Notification> notifications;

    @Transient
    private Double successPercent;

    public void calculateSuccessPercent() {
        if (notifications == null || notifications.isEmpty()) {
            this.successPercent = 0.0;
        } else {
            long successfulNotifications = notifications.stream()
                    .filter(notification -> notification.getStatusType().getCode() == 1)
                    .count();
            this.successPercent = (double) successfulNotifications / notifications.size() * 100;
        }
    }
}
