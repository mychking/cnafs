package com.cnafs.models;

import lombok.*;
import jakarta.persistence.*;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "statustypes")
public class StatusType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "statustypeid")
    private Long statusTypeId;

    @Column(name = "code", nullable = false, unique = true)
    private Integer code;

    @Column(name = "description", nullable = false, length = 50)
    private String description;
}
