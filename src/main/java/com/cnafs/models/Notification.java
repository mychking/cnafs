package com.cnafs.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "customer")
@Table(name = "notifications")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "notificationid")
    private Long notificationId;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "customerid", nullable = false)
    @JsonIgnore
    private Customer customer;

    @OneToOne
    @JoinColumn(name = "notifytypeid", nullable = false)
    private NotifyType notifyType;

    @OneToOne
    @JoinColumn(name = "statustypeid", nullable = false)
    private StatusType statusType;

    @Column(name = "sentdate", nullable = false)
    private LocalDate sentDate;

    @Column(name = "updatedate")
    private LocalDate updateDate;

    @Column(name = "content")
    private String content;

}
