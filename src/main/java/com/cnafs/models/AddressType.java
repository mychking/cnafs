package com.cnafs.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "addresstypes")
public class AddressType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long addresstypeid;

    @Column(name = "code", nullable = false, unique = true)
    private Integer code;

    @Column(name = "description", length = 50)
    private String description;
}
