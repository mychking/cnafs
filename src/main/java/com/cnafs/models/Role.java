package com.cnafs.models;

public enum Role {
    ROLE_SUPER,
    ROLE_ADMIN
}
