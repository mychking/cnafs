package com.cnafs.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "customer")
@Table(name = "addresses")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "addressid")
    private Long addressId;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "customerid", referencedColumnName = "customerid", nullable = false)
    @JsonIgnore
    private Customer customer;

    @Column(name = "addresstypecode", nullable = false)
    private Integer addressTypeCode;

    @Column(name = "addresstext", nullable = false, length = 255)
    private String addressText;
}
