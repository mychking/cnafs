import static org.spockframework.runtime.model.parallel.ExecutionMode.CONCURRENT
import static org.spockframework.runtime.model.parallel.ExecutionMode.SAME_THREAD

runner {
    optimizeRunOrder true
    filterStackTrace true

    parallel {
        enabled true
        defaultSpecificationExecutionMode SAME_THREAD
        defaultExecutionMode SAME_THREAD
        // custom(int parallelism, int minimumRunnable, int maxPoolSize, int corePoolSize, int keepAliveSeconds)
        def parallelism = 2
        def keepAliveSeconds = 60
        custom(parallelism, parallelism, 256 + parallelism, parallelism, keepAliveSeconds)
        // fixed 4
    }
}