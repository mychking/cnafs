package com.cnafs.utils

import groovy.sql.Sql
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.StringUtils
import org.springframework.util.ResourceUtils

@Slf4j
class SqlUtils {

    static def executeScripts(Sql sqlExecutor, String ... scripts) {
        for (script in scripts) {
            executeScript(sqlExecutor, script)
        }
    }

    static def executeScript(Sql sqlExecutor, String script) {
        def sql = ResourceUtils.getFile(script).text
        def stmts = StringUtils.split(sql, ";")
        for (stmt in stmts) {
            if (StringUtils.isNotBlank(stmt)) {
                sqlExecutor.execute(StringUtils.trim(stmt) + ";")
            }
        }
    }
}
