package com.cnafs

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType

@SpringBootApplication(scanBasePackages = "com.cnafs")
@EnableCaching
@ComponentScan(excludeFilters = [
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = CnafsApplication.class)
])
class TestIntegrationApp {

    static void main(String[] args) {
        SpringApplication.run(TestIntegrationApp.class, args)
    }
}
