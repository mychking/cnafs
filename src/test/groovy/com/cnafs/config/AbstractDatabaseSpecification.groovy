package com.cnafs.config

import com.cnafs.utils.SqlUtils
import com.cnafs.TestIntegrationApp
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener
import org.springframework.test.context.support.DirtiesContextTestExecutionListener
import org.springframework.test.context.transaction.TransactionalTestExecutionListener
import spock.lang.Specification

@ContextConfiguration(classes = [
    PostgresContainerConfiguration.class
])
@TestExecutionListeners([
    DependencyInjectionTestExecutionListener,
    DirtiesContextTestExecutionListener,
    TransactionalTestExecutionListener
])
@SpringBootTest(classes = [TestIntegrationApp.class])
@AutoConfigureMockMvc
@ActiveProfiles("test")
@RunWith(SpringRunner)
class AbstractDatabaseSpecification extends Specification {

    def setup() {
    }

    static def sqlScripts(String... scripts) {
        SqlUtils.executeScripts(PostgresContainerConfiguration.getSqlExecutor(), scripts)
    }
}
