package com.cnafs.config

import groovy.sql.Sql
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.configuration.ClassicConfiguration
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.DockerImageName

import javax.sql.DataSource

@Configuration
class PostgresContainerConfiguration {

    private static final String CONTAINER_ID = "postgres:16-alpine"

    private static final String USERNAME = "test-cnafs"
    private static final String PASSWORD = "secret"
    private static final String DB_NAME = "cnafs"

    static def postgres
    static DataSource testDataSource
    static Sql sqlExecutor
    static flyway

    private static def initializeContainer() {
        if (postgres == null) {
            System.setProperty("user.timezone", "GMT")

            postgres = new PostgreSQLContainer<>(DockerImageName.parse(CONTAINER_ID))
                .withUsername(USERNAME)
                .withPassword(PASSWORD)
                .withDatabaseName(DB_NAME)
            postgres.start()

            testDataSource = DataSourceBuilder.create()
                .driverClassName(postgres.getJdbcDriverInstance().getClass().getName())
                .url(postgres.getJdbcUrl())
                .username(USERNAME)
                .password(PASSWORD)
                .build()

            def flywayConfiguration = new ClassicConfiguration()
            flywayConfiguration.setDataSource(testDataSource)

            flyway = new Flyway(flywayConfiguration)
            flyway.migrate()

            sqlExecutor = Sql.newInstance(testDataSource)
        }
    }

    @Bean(name = "testDataSource")
    @Primary
    DataSource getDataSourceTest() throws Exception {
        initializeContainer()
        return testDataSource
    }

    @Bean
    Flyway getFlyway() throws Exception {
        initializeContainer()
        return flyway
    }

    static Sql getSqlExecutor() {
        initializeContainer()
        return sqlExecutor
    }
}
