package com.cnafs.services.impl

import com.cnafs.config.AbstractDatabaseSpecification
import com.cnafs.exceptions.EntityNotFoundException
import com.cnafs.models.Customer
import com.cnafs.services.CustomerService
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.jdbc.Sql

@Slf4j
@Sql(scripts = [
        "classpath:test-data/clear-all.sql"
], executionPhase = Sql.ExecutionPhase.BEFORE_TEST_CLASS)
@Sql(scripts = [
        "classpath:test-data/clear-all.sql"
], executionPhase = Sql.ExecutionPhase.AFTER_TEST_CLASS)
class CustomerServiceImplTest extends AbstractDatabaseSpecification {

    @Autowired
    CustomerService customerService

    def "should create, update and delete customer"() {
        given:
        def generated = generateCustomer()

        when: "find all"
        def customers = customerService.findAllCustomers();

        then:
        customers.size() == 0

        when: "create new customer"
        def saved = customerService.saveCustomer(generated["customer"] as Customer)

        then:
        saved != null
        saved.customerid > 0
        saved.firstname == generated["firstName"]
        saved.lastname == generated["lastName"]

        when: "find all after add"
        def customerId = saved.customerid
        customers = customerService.findAllCustomers()
        def found = customerService.findByID(customerId)

        then:
        customers.size() == 1
        found != null && found.customerid == customerId

        when: "update customer"
        def firstNameUpd = RandomStringUtils.randomAlphanumeric(30)
        def lastNameUpd = RandomStringUtils.randomAlphanumeric(20)
        saved.firstname = firstNameUpd
        saved.lastname = lastNameUpd
        def updated = customerService.updateCustomer(customerId, saved)

        then:
        updated.customerid == customerId
        updated.firstname == firstNameUpd
        updated.lastname == lastNameUpd

        when: "delete customer"
        customerService.deleteCustomer(customerId)
        customerService.findByID(customerId)

        then:
        def ex = thrown(EntityNotFoundException)
        log.info("{}", ex.getMessage())
    }

    private static def generateCustomer() {
        def firstName = RandomStringUtils.randomAlphanumeric(20)
        def lastName = RandomStringUtils.randomAlphanumeric(30)
        def customer = Customer.builder()
            .firstname(firstName)
            .lastname(lastName)
            .build()

        return Map.of(
            "firstName", firstName,
            "lastName", lastName,
            "customer", customer
        )
    }
}
