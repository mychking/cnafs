package com.cnafs.services.impl

import com.cnafs.config.AbstractDatabaseSpecification
import com.cnafs.exceptions.EntityNotFoundException
import com.cnafs.models.Address
import com.cnafs.models.Customer
import com.cnafs.repos.AddressTypeRepository
import com.cnafs.services.AddressService
import com.cnafs.services.CustomerService
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
class AddressServiceImplTest extends AbstractDatabaseSpecification {

    @Autowired
    AddressTypeRepository addressTypeRepository
    @Autowired
    CustomerService customerService
    @Autowired
    AddressService addressService

    def setup() {
        sqlScripts(
            "classpath:test-data/clear-all.sql",
            "classpath:test-data/address-types.sql",
            "classpath:test-data/customers.sql"
        )
    }

    def cleanup() {
        sqlScripts(
            "classpath:test-data/clear-all.sql"
        )
    }

    def "should create, update and delete address"(Integer customerIdx, Integer addressTypeCode) {
        given:
        def customers = customerService.findAllCustomers()
        def addressTypes = addressTypeRepository.findAll()
        def customer = customers[customerIdx - 1] as Customer
        def generated = generateAddress(customer, addressTypeCode)

        when: "find all"
        def addresses = addressService.findAllAddressesByCustomerId(customerIdx);

        then:
        addresses.size() == 0

        when: "create new address"
        def saved = addressService.saveAddress(generated["address"] as Address)

        then:
        saved != null
        saved.addressId > 0
        saved.addressTypeCode != null && saved.addressTypeCode == addressTypeCode
        saved.customer != null && saved.customer.customerid == customer.customerid
        saved.addressText == generated["text"]

        when: "find all after add"
        def addressId = saved.addressId
        addresses = addressService.findAllAddressesByCustomerId(customer.customerid)
        def found = addressService.findAddressById(addressId)

        then:
        addresses.size() == 1
        addresses[0].customer != null && addresses[0].customer.customerid == customer.customerid
        found != null && found.addressId == addressId

        when: "update address"
        def textUpd = RandomStringUtils.randomAlphanumeric(200)
        saved.addressText = textUpd
        def updated = addressService.updateAddress(addressId, saved)

        then:
        updated.addressId == addressId
        updated.addressText == textUpd

        when: "delete address"
        addressService.deleteAddress(addressId)
        addressService.findAddressById(addressId)

        then:
        def ex = thrown(EntityNotFoundException)
        log.info("{}", ex.getMessage())

        where:
        customerIdx | addressTypeCode
        1           | 1
        1           | 2
        1           | 3
        2           | 1
        2           | 2
        2           | 3
        3           | 1
        3           | 2
        3           | 3
    }

    private def generateAddress(Customer customer, Integer addressTypeCode) {
        def text = RandomStringUtils.randomAlphanumeric(100)
        def address = Address.builder()
            .customer(customer)
            .addressTypeCode(addressTypeCode)
            .addressText(text)
            .build()

        return Map.of(
            "customer", customer,
            "addressTypeCode", addressTypeCode,
            "text", text,
            "address", address
        )
    }
}