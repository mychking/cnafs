openapi: 3.0.1
info:
  title: Customer Notification Address Facade System
  description: API for managing customer contact information and preferences for notifications.
  version: 1.0.0
servers:
  - url: /api
tags:
  - name: Customer Management
    description: Operations related to customer data
  - name: Address Management
    description: Operations related to customer addresses
  - name: Preference Management
    description: Operations related to customer notification preferences
  - name: Notification Tracking
    description: Operations related to notification statuses
  - name: Admin Management
    description: Operations related to admin users

paths:
  /customers:
    get:
      tags:
        - Customer Management
      summary: Get a list of all customers with search, filter, and sort options
      parameters:
        - in: query
          name: name
          schema:
            type: string
          required: false
          description: Search customers by name
        - in: query
          name: contactInformation
          schema:
            type: string
          required: false
          description: Search customers by contact information
        - in: query
          name: notificationPreferences
          schema:
            type: string
          required: false
          description: Search customers by notification preferences
        - in: query
          name: sort
          schema:
            type: string
            enum: [name, contactInformation, notificationPreferences]
          required: false
          description: Sort customers by specified field
        - in: query
          name: order
          schema:
            type: string
            enum: [asc, desc]
          required: false
          description: Order of sorting (ascending or descending)
      responses:
        '200':
          description: A list of customers
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Customer'
      security:
        - bearerAuth: []
    post:
      tags:
        - Customer Management
      summary: Add a new customer
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Customer'
      responses:
        '201':
          description: Customer added successfully
      security:
        - bearerAuth: []
  /customers/{customerId}:
    get:
      tags:
        - Customer Management
      summary: Get customer details
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to retrieve
      responses:
        '200':
          description: Customer details
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Customer'
      security:
        - bearerAuth: []
    put:
      tags:
        - Customer Management
      summary: Update customer details
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to update
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Customer'
      responses:
        '200':
          description: Customer updated successfully
      security:
        - bearerAuth: []
    delete:
      tags:
        - Customer Management
      summary: Remove a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to remove
      responses:
        '200':
          description: Customer removed successfully
      security:
        - bearerAuth: []

  /customers/{customerId}/addresses:
    get:
      tags:
        - Address Management
      summary: Get all addresses for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to retrieve addresses for
      responses:
        '200':
          description: A list of addresses
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Address'
      security:
        - bearerAuth: []
    post:
      tags:
        - Address Management
      summary: Add a new address for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to add an address for
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Address'
      responses:
        '201':
          description: Address added successfully
      security:
        - bearerAuth: []

  /customers/{customerId}/addresses/{addressId}:
    put:
      tags:
        - Address Management
      summary: Update an address for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to update address for
        - in: path
          name: addressId
          schema:
            type: integer
          required: true
          description: ID of the address to update
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Address'
      responses:
        '200':
          description: Address updated successfully
      security:
        - bearerAuth: []
    delete:
      tags:
        - Address Management
      summary: Remove an address for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to remove address for
        - in: path
          name: addressId
          schema:
            type: integer
          required: true
          description: ID of the address to remove
      responses:
        '200':
          description: Address removed successfully
      security:
        - bearerAuth: []

  /customers/{customerId}/preferences:
    get:
      tags:
        - Preference Management
      summary: Get all notification preferences for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to retrieve preferences for
      responses:
        '200':
          description: A list of preferences
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Preference'
      security:
        - bearerAuth: []
    post:
      tags:
        - Preference Management
      summary: Add a new preference for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to add a preference for
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Preference'
      responses:
        '201':
          description: Preference added successfully
      security:
        - bearerAuth: []

  /customers/{customerId}/preferences/{preferenceId}:
    put:
      tags:
        - Preference Management
      summary: Update a preference for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to update preference for
        - in: path
          name: preferenceId
          schema:
            type: integer
          required: true
          description: ID of the preference to update
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Preference'
      responses:
        '200':
          description: Preference updated successfully
      security:
        - bearerAuth: []
    delete:
      tags:
        - Preference Management
      summary: Remove a preference for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to remove preference for
        - in: path
          name: preferenceId
          schema:
            type: integer
          required: true
          description: ID of the preference to remove
      responses:
        '200':
          description: Preference removed successfully
      security:
        - bearerAuth: []

  /customers/notifications:
    get:
      tags:
        - Notification Tracking
      summary: Get a list of all notifications
      responses:
        '200':
          description: A list of notifications
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/NotificationStatus'
      security:
        - bearerAuth: []

  /customers/{customerId}/notifications:
    get:
      tags:
        - Notification Tracking
      summary: Get all notifications for a customer
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: ID of the customer to retrieve notifications for
      responses:
        '200':
          description: A list of notifications for the customer
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/NotificationStatus'
      security:
        - bearerAuth: []

  /users:
    get:
      tags:
        - Admin Management
      summary: Get a list of all admins
      responses:
        '200':
          description: A list of admins
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Admin'
      security:
        - bearerAuth: []

components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    Customer:
      type: object
      properties:
        customerId:
          type: integer
        firstName:
          type: string
        lastName:
          type: string
        addresses:
          type: array
          items:
            $ref: '#/components/schemas/Address'
        preferences:
          type: array
          items:
            $ref: '#/components/schemas/Preference'

    Address:
      type: object
      properties:
        addressId:
          type: integer
        customerId:
          type: integer
        addressTypeCode:
          type: integer
        addressText:
          type: string

    Preference:
      type: object
      properties:
        preferenceId:
          type: integer
        customerId:
          type: integer
        notifyTypeCode:
          type: integer

    NotificationStatus:
      type: object
      properties:
        notificationId:
          type: integer
        customerId:
          type: integer
        notifyTypeCode:
          type: integer
        statusTypeCode:
          type: integer
        sentDate:
          type: string
          format: date-time
        updateDate:
          type: string
          format: date-time
        content:
          type: string

    Admin:
      type: object
      properties:
        adminId:
          type: integer
        username:
          type: string
        password:
          type: string
        userRole:
          type: string
